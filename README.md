# POC: React Server-side Rendering

This is the proof of concept of server-side renderd react application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites

What things you need to install the software:

```
Node
```


### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
yarn
```
or
```
npm install
```
and then 

```
yarn dev
```
or
```
npm dev
```
End with an example of getting some data out of the system or using it for a little demo

## Built With

* [React](https://reactjs.org/) - The web framework used
* [Webpack](https://webpack.js.org/) - Dependency Management
* [Express](https://expressjs.com/) - Used to generate RSS Feeds


