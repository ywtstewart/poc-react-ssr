import React from "react";

export default class Welcome extends React.Component {
    constructor() {
        super();
        this.state = {
            title: "Welcome to React SSR!",
        };

        this.showNewMessage = this.showNewMessage.bind( this );
    }

    showNewMessage() {
        this.setState( {
            title: "This is renderd in the Welcome component.",
        } );
    }

    render() {
        return (
            <div>
                <h1>{this.state.title}</h1>
                <button onClick={ this.showNewMessage }>Test</button>
            </div>
        );
    }
}
